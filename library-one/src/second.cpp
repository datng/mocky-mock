#include <one/second.hpp>

#include <iostream>

namespace library_one {
second::second() {
    m_library_name = "one";
    m_class_number = 2;
}

second::~second() {
    std::cout << "Deleting object of class library_one::second" << std::endl;
}

void
second::function_void() {
    std::cout << "function library_one::second::function_void() was called."
              << std::endl;
}

int
second::function_int() {
    std::cout << "function library_one::second::function_int() was called."
              << std::endl;
    return m_class_number;
}

std::string
second::function_string() {
    std::cout << "function library_one::second::function_string() was called."
              << std::endl;
    return m_library_name;
}
} // namespace library_one
