#include <one/first.hpp>

#include <iostream>

namespace library_one {
first::first() {
    m_library_name = "one";
    m_class_number = 1;
}

first::~first() {
    std::cout << "Deleting object of class library_one::first" << std::endl;
}

void
first::function_void() {
    std::cout << "function library_one::first::function_void() was called."
              << std::endl;
}

int
first::function_int() {
    std::cout << "function library_one::first::function_int() was called."
              << std::endl;
    return m_class_number;
}

std::string
first::function_string() {
    std::cout << "function library_one::first::function_string() was called."
              << std::endl;
    return m_library_name;
}
} // namespace library_one
