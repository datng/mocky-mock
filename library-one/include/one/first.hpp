#ifndef ONE_FIRST_HPP
#define ONE_FIRST_HPP
#include "common.hpp"

#include <string>

namespace library_one {
class first {
public:
    first();
    ~first();
    void function_void();
    int function_int();
    std::string function_string();

private:
    int m_class_number;
    std::string m_library_name;
};
} // namespace library_one
#endif // ONE_FIRST_HPP
