#!/bin/bash

if [ ! -d "build" ]; then
    mkdir build
fi

cd build

# only cmake 2.8 is available on CentOS Linux 7
# cmake 3.13 can be installed from the EPEL repository, but it is called cmake3
if [[ $(cat /etc/os-release | grep "CentOS Linux 7") != "" ]]; then
    cmake3 .. || { exit 1; }
else
    cmake .. || { exit 1; }
fi

make || { exit 1; }
cd ..
