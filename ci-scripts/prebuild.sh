#!/bin/sh
# check runner system
if [[ $(cat /etc/os-release | grep "CentOS Linux") != "" ]]; then
    cmake3 --version || { exit 1; }
else
    cmake --version || { exit 1; }
fi
doxygen -v
g++ --version
lcov -v
pip3 -V
uname -a



# install external dependencies
if [ ! -d external ]; then
    mkdir external
fi
cd external

# googletest
if [ ! -f release-1.8.1.zip ]; then
    wget https://github.com/google/googletest/archive/release-1.8.1.zip -O googletest-1.8.1.zip
fi
unzip googletest-1.8.1.zip
