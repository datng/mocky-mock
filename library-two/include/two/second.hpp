#ifndef TWO_SECOND_HPP
#define TWO_SECOND_HPP
#include "common.hpp"

#include <string>

namespace library_two {
class second {
public:
    second();
    ~second();
    void function_void();
    int function_int();
    std::string function_string();

private:
    int m_class_number;
    std::string m_library_name;
};
} // namespace library_two
#endif // TWO_SECOND_HPP
