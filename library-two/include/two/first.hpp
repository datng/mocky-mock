#ifndef TWO_FIRST_HPP
#define TWO_FIRST_HPP
#include "common.hpp"

#include <string>

namespace library_two {
class first {
public:
    first();
    ~first();
    void function_void();
    int function_int();
    std::string function_string();

private:
    int m_class_number;
    std::string m_library_name;
};
} // namespace library_two
#endif // TWO_FIRST_HPP
