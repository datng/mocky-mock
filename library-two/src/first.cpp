#include <two/first.hpp>

#include <iostream>

namespace library_two {
first::first() {
    m_library_name = "two";
    m_class_number = 1;
}

first::~first() {
    std::cout << "Deleting object of class library_two::first" << std::endl;
}

void
first::function_void() {
    std::cout << "function library_two::first::function_void() was called."
              << std::endl;
}

int
first::function_int() {
    std::cout << "function library_two::first::function_int() was called."
              << std::endl;
    return m_class_number;
}

std::string
first::function_string() {
    std::cout << "function library_two::first::function_string() was called."
              << std::endl;
    return m_library_name;
}
} // namespace library_two
