#include <iostream>
#include <one/first.hpp>
#include <one/second.hpp>
#include <two/first.hpp>
#include <two/second.hpp>

int
main() {
    library_one::first f1;
    library_two::first f2;

    library_one::second s1;
    library_two::second s2;

    f1.function_int();
    f1.function_string();
    f1.function_void();
    std::cout << std::endl;

    f2.function_int();
    f2.function_string();
    f2.function_void();
    std::cout << std::endl;

    s1.function_int();
    s1.function_string();
    s1.function_void();
    std::cout << std::endl;

    s2.function_int();
    s2.function_string();
    s2.function_void();
    std::cout << std::endl;

    return 0;
}
