#include "library-one.test/first.test.hpp"
#include "library-one.test/second.test.hpp"

#include "library-two.test/first.test.hpp"
#include "library-two.test/second.test.hpp"

int
main(int argc, char* argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
