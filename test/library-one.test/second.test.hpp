#ifndef MOCKY_MOCK_ONE_SECOND_TEST_HPP
#define MOCKY_MOCK_ONE_SECOND_TEST_HPP
#include <gtest/gtest.h>

#include <one/second.hpp>

TEST(one_second, function_int) {
    library_one::second s;
    EXPECT_EQ(s.function_int(), 2);
}

TEST(one_second, function_string) {
    library_one::second s;
    EXPECT_EQ(s.function_string(), "one");
}

#endif // MOCKY_MOCK_ONE_SECOND_TEST_HPP
