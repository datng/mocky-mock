#ifndef MOCKY_MOCK_ONE_FIRST_TEST_HPP
#define MOCKY_MOCK_ONE_FIRST_TEST_HPP
#include <gtest/gtest.h>

#include <one/first.hpp>

TEST(one_first, function_int) {
    library_one::first f;
    EXPECT_EQ(f.function_int(), 1);
}

TEST(one_first, function_string) {
    library_one::first f;
    EXPECT_EQ(f.function_string(), "one");
}

#endif // MOCKY_MOCK_ONE_FIRST_TEST_HPP
