#ifndef MOCKY_MOCK_TWO_SECOND_TEST_HPP
#define MOCKY_MOCK_TWO_SECOND_TEST_HPP
#include <gtest/gtest.h>

#include <two/second.hpp>

TEST(two_second, function_int) {
    library_two::second s;
    EXPECT_EQ(s.function_int(), 2);
}

TEST(two_second, function_string) {
    library_two::second s;
    EXPECT_EQ(s.function_string(), "two");
}

#endif // MOCKY_MOCK_TWO_SECOND_TEST_HPP
