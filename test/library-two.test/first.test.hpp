#ifndef MOCKY_MOCK_TWO_FIRST_TEST_HPP
#define MOCKY_MOCK_TWO_FIRST_TEST_HPP
#include <gtest/gtest.h>

#include <two/first.hpp>

TEST(two_first, function_int) {
    library_two::first f;
    EXPECT_EQ(f.function_int(), 1);
}

TEST(two_first, function_string) {
    library_two::first f;
    EXPECT_EQ(f.function_string(), "two");
}

#endif // MOCKY_MOCK_TWO_FIRST_TEST_HPP
